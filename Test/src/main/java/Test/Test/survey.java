package Test.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class survey {
	@Test(invocationCount=250)
	public void survey() throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\rdx2\\Downloads\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://forms.gle/2PXqo7SLuTfeYii2A");
		
		driver.manage().window().maximize();
		Thread.sleep(1500);
		//WebDriverWait wait = new WebDriverWait(driver, 3);
		//WebElement element = wait.until(
		//ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\\\"mG61Hd\\\"]/div[2]/div/div[2]/div/div/div/div[2]/div/div[1]/div/div[1]/input")));
		driver.findElement(By.xpath("//*[@id=\"mG61Hd\"]/div[2]/div/div[2]/div/div/div/div[2]/div/div[1]/div/div[1]/input")).sendKeys("test");
		driver.findElement(By.xpath("//*[@id=\"mG61Hd\"]/div[2]/div/div[3]/div[1]/div/div/span")).click();
		
		Thread.sleep(1500);
		WebElement ele = driver.findElement(By.xpath("//*[@id=\"i6\"]/div[3]"));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", ele);
		
		WebElement ele1 = driver.findElement(By.xpath("//*[@id=\"i9\"]/div[3]"));
		JavascriptExecutor jse1 = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", ele1);
		
		WebElement ele2 = driver.findElement(By.xpath("//*[@id=\"i12\"]/div[3]"));
		JavascriptExecutor jse2 = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", ele2);
		
		WebElement ele3 = driver.findElement(By.xpath("//*[@id=\"i15\"]/div[3]"));
		JavascriptExecutor jse3 = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", ele3);
		
		WebElement ele4 = driver.findElement(By.xpath("//*[@id=\"i18\"]/div[3]"));
		JavascriptExecutor jse4 = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", ele4);
		
		WebElement ele5 = driver.findElement(By.xpath("//*[@id=\"i64\"]/div[3]/div/div"));
		JavascriptExecutor jse5 = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", ele5);
		
		driver.findElement(By.xpath("//*[@id=\"mG61Hd\"]/div[2]/div/div[2]/div[4]/div/div/div[2]/div/div[1]/div/div[1]/input")).sendKeys("this is a test");
		driver.findElement(By.xpath("//*[@id=\"mG61Hd\"]/div[2]/div/div[3]/div[1]/div/div[2]/span/span")).click();
		
		driver.close();
	}

}
