package Test.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TestingALink {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\rdx2\\Downloads\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.testandquiz.com/selenium/testing.html");
		driver.manage().window().maximize();
		
		System.out.println(driver.getTitle());
		
		String s = driver.findElement(By.tagName("b")).getText();
		
		System.out.println(s);
		
		//driver.findElement(By.linkText("This is a link")).click();
		
		//driver.navigate().back();
		
		driver.findElement(By.xpath("//*[@id=\"idOfButton\"]")).click();
		
		driver.findElement(By.id("male")).click();
		
		driver.findElement(By.className("Automation")).click();
		
		Select drop = new Select(driver.findElement(By.id("testingDropdown")));
		
		drop.selectByValue("Manual");
		
		driver.findElement(By.xpath("/html/body/div/div[11]/div/p/button")).click();
		
		String alertText = driver.switchTo().alert().getText();
		System.out.println(alertText);
		
		driver.switchTo().alert().accept();
		
		driver.findElement(By.xpath("/html/body/div/div[12]/div/p[1]/button")).click();
		
		driver.switchTo().alert().dismiss();
		
		System.out.println(driver.findElement(By.xpath("//*[@id=\"demo\"]")).getText());
		
		Thread.sleep(3000);
		
		driver.close();
		
	}

}
