package Test.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws InterruptedException, IOException
    {
    	System.setProperty("webdriver.chrome.driver", "C:\\Users\\rdx2\\Downloads\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.google.com/"); 
		System.out.println(driver.getTitle());
		driver.manage().window().maximize();
		
		//finding element
		//driver.findElement(By.cssSelector("input")).sendKeys("iam stuck");
		//driver.findElement(By.tagName("input")).sendKeys("iam stuck");
		//driver.findElement(By.xpath("/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input")).sendKeys("iam stuck");
		
		
		//finding all hrefs
		/*
		List<WebElement> webelement = driver.findElements(By.tagName("a"));
		
		for(WebElement link : webelement) {
			System.out.println(link.getAttribute("href"));
		}
		*/
		//screenshot 
		/*TakesScreenshot ss = ((TakesScreenshot)driver);
		File srcfile = ss.getScreenshotAs(OutputType.FILE);
		
		File destfile = new File("D:\\selenium\\"+driver.getTitle()+".png");
		FileUtils.copyFile(srcfile,destfile);
		*/
		
		
		Thread.sleep(3000);
		driver.close();
		
    }
}
