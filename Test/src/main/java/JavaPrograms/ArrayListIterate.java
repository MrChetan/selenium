package JavaPrograms;

import java.util.*;

public class ArrayListIterate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> l = new ArrayList<String>();

		l.add("chr");
		l.add("dasde");
		l.add("ass");

		// Simple For loop
		System.out.println("Simple For loop");
		for (int i = 0; i < l.size(); i++) {
			System.out.println(l.get(i));
		}

		// New Enhanced For loop
		System.out.println("New Enhanced For loop");
		for (String temp : l) {
			System.out.println(temp);
		}

		// Iterator
		System.out.println("Iterator");
		Iterator<String> it = l.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}

		// list iterator
		System.out.println("list iterator");
		ListIterator<String> lit = l.listIterator();
		while (lit.hasNext()) {
			System.out.println(lit.next());
		}

		// while loop
		System.out.println("while loop");
		int x = 0;
		while (x < l.size()) {
			System.out.println(l.get(x));
			x++;
		}
		// Iterable.forEach() using lambda functions
		System.out.println("Iterable.forEach() using lambda functions");
		l.forEach((temp) -> {
			System.out.println(temp);
		});

		// collection Stream.forEach()
		System.out.println("collection Stream.forEach() using lambda functions");
		l.stream().forEach((temp) -> {
			System.out.println(temp);
		});

	}

}
