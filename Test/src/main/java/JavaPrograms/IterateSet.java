package JavaPrograms;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class IterateSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Set<String> s = new HashSet<String>();

		s.add("asd");
		s.add("sdf");
		s.add("dgh");

		System.out.println(s);

		for (String temp : s) {
			System.out.println(temp);
		}

		Iterator<String> it = s.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}

	}

}
